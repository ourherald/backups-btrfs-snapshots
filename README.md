btrfs-snapshots.sh
BTRFS snapshot-based backup script.
2019-11-02: Tim Calabro

Script, scheduled through cron, should take regular backups of specified BTRFS subvolumes
which can then be used by Duplicati (or any other remote syncing platform) for syncing to remote backup sites.
The script will create aliases to the most recent backup and an LTS backup.

Set variables for source, destination, and days until pruning in the script before using the script.
To do this, copy the included env.conf.example file to env.conf and make the entries there.
