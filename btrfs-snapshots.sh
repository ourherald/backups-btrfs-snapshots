#!/bin/bash
#
# btrfs-snapshots.sh
# BTRFS snapshot-based backup script.
# 2019-11-02: Tim Calabro

# Script, scheduled through cron, should take regular backups of specified BTRFS subvolumes
# which can then be used by Duplicati (or any other remote syncing platform) for syncing to remote backup sites.
# The script will create aliases to the most recent backup and an LTS backup.

# Variables

## Customizable Variables
# This sources the necessary variables from a separate env.conf file
envdir=/root/scripts/backups-btrfs-snapshots
#envdir=$(dirname $0)
source "$envdir"/env.conf
logfile=/var/log/btrfs-snapshots.log

### Create a function to echo output and to write to a log file

writelog() {    # Take arguments and write them to logfile (and echo them out)
    echo "${1}" "${2}" "${3}" "${4}"
    echo "$(date)" "${1}"   "${2}" "${3}" "${4}" >> "$logfile"
}

## Auto variables
datestamp="$(date +%Y-%m-%d-%H%M)"

## User input
options=$1
helpnote="This script offers the following flags:
	--help	: Displays this help message
	--debug	: Displays debugging information using set -x
"

# Help menu
if [[ $options == "--help" ]]; then
	echo "$helpnote"
	exit 0
elif [[ $options == "--debug" ]]; then
	set -x
fi

# Make sure the necessary variables are set
if [[ $sourceDir == "" ]]; then
    echo "Looks like you didn't set a source directory in the script. Open it in an editor and check the variables."
    exit 1
elif [[ $snapDir == "" ]]; then
    echo "Looks like you didn't set a snapshot directory in the script. Open it in an editor and check the variables."
    exit 1
elif [[ $oldDays == "" ]]; then
    echo "Looks like you didn't set a time limit for snapshots to live in the script. Open it in an editor and check the variables."
    exit 1
fi

# make new snapshot
for item in "${sourceDir[@]}"; do
    dirName=$(basename $item)
    snapName="$snapPrefix-$dirName-$datestamp"
    snapDest="$snapDir/$snapName"
    hourlyPattern="$snapPrefix-[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{2}00"
    ltpattern1="$snapPrefix-[0-9]{4}-[0-9]{2}-[0-9]{2}-1800"
    ltpattern2="$snapPrefix-[0-9]{4}-[0-9]{2}-[0-9]{2}-1200"
    /sbin/btrfs subvolume snapshot -r "$item" "$snapDest" &&
    writelog "Created new snapshot at $snapDest"

    # remove old current symlink
    if [[ -L "$snapDir/current-$dirName" ]]; then
    	rm "$snapDir/current-$dirName" &&
    	writelog "Removed old symlink for current-$dirName"
    fi
    # create "current" symlink
    ln -s "$snapDest" "$snapDir/current-$dirName" &&
    writelog "$snapDest is now set as CURRENT"

    # create hourly and longterm symlinks
    if [[ "$snapName" =~ $hourlyPattern ]]; then
    	# remove old symlink
    	if [[ -L "$snapDir/hourly-$dirName" ]]; then
    		rm "$snapDir/hourly-$dirName" &&
    		writelog "Removed old symlink for hourly-$dirName"
        fi
        # make the hourly symlink
        ln -s "$snapDest" "$snapDir/hourly-$dirName" &&
        writelog "$snapName is now set as HOURLY" &&
        # create longer-lasting symlink
        if [[ "$snapName" =~ $ltpattern1 ]]; then
    	    # remove old longterm symlink
    	    if [[ -L "$snapDir/longterm-$dirName" ]]; then
    		    rm "$snapDir/longterm-$dirName" &&
    		    writelog "Removed old symlink to longterm-$dirName"
            fi
    	    # make the new symlink
    	    ln -s "$snapDest" "$snapDir/longterm-$dirName" &&
    		writelog "$snapDest is now set as LONGTERM"
        elif [[ "$snapName" =~ $ltpattern2 ]]; then
    	    # remove old longterm symlink
    	    if [[ -L "$snapDir/longterm-$dirName" ]]; then
    		    rm "$snapDir/longterm-$dirName" &&
    		    writelog "Removed old symlink to longterm-$dirName"
    	    fi
        # make the new symlink
        ln -s "$snapDest" "$snapDir/longterm-$dirName" &&
        writelog "$snapDest is now set as LONGTERM"
        else
    	writelog "$snapName will not be used for LONGTERM"
        fi
    else
    	writelog "$snapName will not be used for HOURLY or LONGTERM"
    fi
done


# prune old snapshots
find $snapDir/ -maxdepth 0 -mtime +$oldDays -exec /sbin/btrfs subvolume delete {} \;

# cleanup logfile
tail -n 1000 $logfile > /tmp/tmp-btrfs-log
cp -f /tmp/tmp-btrfs-log $logfile && rm /tmp/tmp-btrfs-log

exit 0
